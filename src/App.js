import Template01 from "./templates/template_01/template_01";
// import Template02 from "./templates/template_02/template_02";
// import Template03 from "./templates/template_03/template_03";
// import Template04 from "./templates/template_04/template_04";
// import Template05 from "./templates/template_05/template_05";
// import Template06 from "./templates/template_06/template_06";
// import Template07 from "./templates/template_07/template_07";
import "./App.css";

const Data = {
  // Done
  Educations: [
    {
      id_: "1414",
      Degree: "Bachelor",
      Field: "Software Engineering",
      Faculty: "Faculty Name",
      UniversityName: "Al-Baath University",
      DegreeFrom5: 3.76,
      DegreeFrom100: 82,
      Grade: "Excellent",
      YearStart: 2014,
      YearEnd: 2019,
    },
    {
      id_: "1",
      Degree: "Bachelor",
      Field: "Software Engineering",
      Faculty: "Faculty Name",
      UniversityName: "Al-Baath University",
      DegreeFrom5: 3.76,
      DegreeFrom100: 82,
      Grade: "Excellent",
      YearStart: 2014,
      YearEnd: 2019,
    },
  ],
  Experiences: [
    {
      id_: "q",
      Name: "Job Title",
      Description: "Job Description",
      Start: 2014,
      End: 2016,
      Project: "Project Title",
    },
    {
      id_: "q4",
      Name: "Job Title",
      Description: "Job Description",
      Start: 2014,
      End: 2016,
      Project: "Project Title",
    },
  ],
  // Done
  Courses: [
    {
      id_: "245",
      Name: "Course Name",
      Description: "Course Description",
      Year: "2015",
    },
    {
      id_: "2115",
      Name: "Course Name",
      Description: "Course Description",
      Year: "2015",
    },
  ],
  // Done
  Certificates: [
    {
      id_: "25",
      Name: "Certificate Name",
      Description: "Certificate Description",
      Year: "2015",
    },
  ],
  // Done
  Languages: [
    {
      id_: "546",
      Name: "Arabic",
      Rate: 3,
      RateFrom10: 6,
      RateFrom100: 50,
    },
    {
      id_: "56",
      Name: "Arabic",
      Rate: 3,
      RateFrom10: 6,
      RateFrom100: 79,
    },
  ],
  // Done
  Memberships: [
    {
      id_: "7849",
      Name: "Membership Name",
    },
    {
      id_: "789",
      Name: "Membership Name",
    },
  ],
  // Done
  OtherTrainings: [
    {
      id_: "855",
      Name: "Course Name",
    },
    {
      id_: "8755",
      Name: "Course Name",
    },
  ],
  // Done
  TechnicalSkills: [
    {
      id_: "2215",
      Name: "Technical Skill Name",
      Rate: 85,
    },
    {
      id_: "2414",
      Name: "Technical Skill Name",
      Rate: 49,
    },
  ],
  // Done
  Skill: [
    {
      id_: "141",
      Name: "MS1",
    },
    {
      id_: "145",
      Name: "MS3",
    },
    {
      id_: "149",
      Name: "MS2",
    },
  ],
  // Done
  References: [
    {
      id_: "123",
      Name: "Reference Name",
      Number: "05000000",
    },
  ],
  // Done
  PersonalInformation: {
    id_: "String",
    FirstName: "Mohammed",
    LastName: "Saad",
    Phone: "050000000",
    Email: "xxxxxx@hotmail.com",
    LinkedIn: "String",
    MaritalStatus: "Married",
    Nationality: "Saudi",
    Birth: "20/10/1991",
    City: "Al-Riyadh",
  },
  // Done
  CareerObjectives: {
    id_: "3354",
    Text:
      "Lorem ipsum dolor sit amet consectetur adipisicing elit. Unde, mollitia recusandae? Qui numquam alias at.",
  },
};

function App() {
  const language = "Ar";
  return (
    <div className="App">
      <Template01 Data={Data} language={language} />
      {/* <Template02 Data={Data} /> */}
      {/* <Template03 Data={Data} /> */}
      {/* <Template04 Data={Data} /> */}
      {/* <Template05 Data={Data} /> */}
      {/* <Template06 Data={Data} /> */}
      {/* <Template07 Data={Data} /> */}
    </div>
  );
}

export default App;
